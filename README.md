# EAT-ME-BALL-GAME

## Installation
`npm install`

### For Device Motion
`ionic cordova plugin add cordova-plugin-device-motion`
`npm install --save @ionic-native/device-motion`

### For Native Audio
`ionic cordova plugin add cordova-plugin-nativeaudio`
`npm install --save @ionic-native/native-audio`

### For debugging in browser
`ionic serve` It will not properly on browser because of native ionic libraries

### For debugging in android
`ionic cordova run android --debug --device`
