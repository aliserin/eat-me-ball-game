import { PointModel } from "../models/point.model";
import { Injectable } from "@angular/core";

@Injectable()
export class Helper {
    calculateDistanceBetweenTwoPoints(p1: PointModel, p2: PointModel): number {
        return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2));
    }
}