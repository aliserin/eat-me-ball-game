import { Injectable } from "@angular/core";
import { BallModel } from "../models/ball.model";
import { IsSticked } from "../models/isSticked.model";
import { CheckBordersResponseModel } from "../models/checkBordersResponse.model";
import { Helper } from "../shared/helper";

@Injectable()

export class CheckObstacleService {
    constructor(private helper: Helper) {

    }
    isSticked: IsSticked = {
        left: false,
        right: false,
        top: false,
        bottom: false,
    }

    // It checks collision of the ball to walls
    checkBorders(ball: BallModel, width: number, height: number): CheckBordersResponseModel {
        let checkBordersResponse: CheckBordersResponseModel = {
            ball: ball,
            collisionCounter: 0
        }

        if (ball.point.x - ball.r <= 0) {
            ball.velocity.x = 0;
            ball.point.x = ball.r;
            if(!this.isSticked.left) {
                checkBordersResponse.collisionCounter += 1;
                this.isSticked.left = true;
            }
        } else {
            this.isSticked.left = false;
        }
        if (ball.point.x + ball.r >= width) {
            ball.velocity.x = 0;
            ball.point.x = width - ball.r;
            if(!this.isSticked.right) {
                checkBordersResponse.collisionCounter += 1;
                this.isSticked.right = true;
            }
        } else {
            this.isSticked.right = false;
        }
        if (ball.point.y - ball.r <= 0) {
            ball.point.y = ball.r;
            ball.velocity.y = 0;
            if(!this.isSticked.top) {
                checkBordersResponse.collisionCounter += 1;
                this.isSticked.top = true;
            }
        } else {
            this.isSticked.top = false;
        }
        if (ball.point.y + ball.r >= height) {
            ball.point.y = height - ball.r;
            ball.velocity.y = 0;
            if(!this.isSticked.bottom) {
                checkBordersResponse.collisionCounter += 1;
                this.isSticked.bottom = true;
            }
        } else {
            this.isSticked.bottom = false;
        }
        checkBordersResponse.ball = ball;
        return checkBordersResponse;
    }

    // checks our ball collides to random ball
    isFoodBallEated(ourBall: BallModel, foodBall: BallModel): boolean {
        let distance = this.helper.calculateDistanceBetweenTwoPoints(ourBall.point, foodBall.point);
        if(distance < (ourBall.r + foodBall.r)) {
            return true;
        }
        return false;
    }
}