import { Injectable } from "@angular/core";
import { BallModel } from "../models/ball.model";
import { Helper } from "../shared/helper";

@Injectable()

export class DrawingService {
    constructor(private helper: Helper) {

    }

    drawBall(ctx: any, ball: BallModel) {
        ctx.beginPath();
        ctx.arc(ball.point.x, ball.point.y, ball.r, 0, Math.PI * 2);
        ctx.fillStyle = ball.color;
        ctx.fill();
    }

    // it creates random ball outsite of our ball (The corner area that our ball cannot reach did not calculated. It can be added)
    createRandomBall(ourBall, width, height) {
        let x, y;
        let color = 'black'

        while (true) {
            x = Math.round(Math.random() * (width - 3));
            y = Math.round(Math.random() * (height - 3));

            // accept random point if it is not so close the walls
            while (true) {
                y = Math.round(Math.random() * height);
                if (y > 5 && y < (height - 5)) {
                    break;
                }
            }

            while (true) {
                x = Math.round(Math.random() * width);
                if (x > 5 && x < (width - 5)) {
                    break;
                }
            }

            let randomBall: BallModel = {
                point: {
                    x: x,
                    y: y
                },
                color: color,
                r: 3,
            }

            // check random ball is not on our ball
            let d = this.helper.calculateDistanceBetweenTwoPoints(ourBall.point, randomBall.point)
            if (d > (ourBall.r + randomBall.r)) {
                return randomBall;
            }
        }
    }
}