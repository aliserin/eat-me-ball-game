import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { ChallengePage } from "../challenge/challenge";
import { EatMeGamePage } from "../eat-me-game/eat-me-game";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    constructor(private nav: NavController) {
        
    }
    
    onChallengeClicked() {
        this.nav.setRoot(ChallengePage);
    }

    onEatMeClicked() {
        this.nav.setRoot(EatMeGamePage);
    }
}