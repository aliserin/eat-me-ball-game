import { Component, AfterViewInit } from "@angular/core";
import { Storage } from '@ionic/storage';
import { SettingsModel } from "../../models/settings.model";
import { NavController } from "ionic-angular";
import { HomePage } from "../home/home";

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
})
export class SettingsPage implements AfterViewInit {
    settings: SettingsModel = new SettingsModel();

    constructor(private storage: Storage,
        private nav: NavController) {
        
    }

    ngAfterViewInit(): void {
        this.getSettings();
    }

    // set settings to local db
    updateSettings() {
        this.storage.set('settings', this.settings)
            .then(result => {
                this.nav.setRoot(HomePage);
            })
            .catch(errror => {

            });
    }

    // get settings from local db
    getSettings() {
        this.storage.get('settings')
            .then(result => {
                if (result !== null) {
                    this.settings = result;
                }
            });
    }
}
