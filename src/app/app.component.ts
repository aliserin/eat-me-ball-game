import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { SettingsModel } from '../models/settings.model';
import { SettingsPage } from '../pages/settings/settings';
import { ChallengePage } from '../pages/challenge/challenge';
import { EatMeGamePage } from '../pages/eat-me-game/eat-me-game';
import { HomePage } from '../pages/home/home';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;
	rootPage: any = HomePage;
	pages: Array<{ title: string, component: any }>;
	defaultSettings: SettingsModel = {
		gravity: 0.025,
		ballSize: 10,
		ballColor: 'blue',
		health: 5,
		isSoundOpen: true
	};
	highScore: 0;

	constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public storage: Storage) {
		this.initializeApp();

		// used for an example of ngFor and navigation
		this.pages = [
			{ title: 'Challenge', component: ChallengePage },
			{ title: 'Eat Me', component: EatMeGamePage },
			{ title: 'Settings', component: SettingsPage }
		];

	}

	initializeApp() {
		this.platform.ready().then(() => {
			this.setDefaultSettingsIfNoSettings();
			this.statusBar.styleDefault();
			this.splashScreen.hide();
		});
	}

	openPage(page) {
		this.nav.setRoot(page.component);
	}

	// set settings for very first time launch
	setDefaultSettingsIfNoSettings() {
		this.storage.get('settings')
			.then(result => {
				if (result === null) {
					this.storage.set('settings', this.defaultSettings)
						.catch(error => {

						})
				}
			})
			.catch(error => {

			});
	}
}
