import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { DrawingService } from '../services/drawing.service';
import { CheckObstacleService } from '../services/checkObstacle.service';
import { DeviceMotion } from '@ionic-native/device-motion';
import { SettingsPage } from '../pages/settings/settings';
import { IonicStorageModule } from '@ionic/storage';
import { ChallengePage } from '../pages/challenge/challenge';
import { ChallengeComponent } from '../components/challenge/challenge.component';
import { EatMeGamePage } from '../pages/eat-me-game/eat-me-game';
import { EatMeGameComponent } from '../components/eat-me-game/eat-me-game.component';
import { Helper } from '../shared/helper';
import { HomePage } from '../pages/home/home';
import { NativeAudio } from '@ionic-native/native-audio';

@NgModule({
	declarations: [
		MyApp,
		ChallengePage,
		ChallengeComponent,
		SettingsPage,
		EatMeGamePage,
		EatMeGameComponent,
		HomePage
	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp),
		IonicStorageModule.forRoot(),
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		ChallengePage,
		ChallengeComponent,
		SettingsPage,
		EatMeGamePage,
		EatMeGameComponent,
		HomePage
	],
	providers: [
		StatusBar,
		SplashScreen,
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		DrawingService,
		CheckObstacleService,
		Helper,
		DeviceMotion,
		NativeAudio
	]
})
export class AppModule { }
