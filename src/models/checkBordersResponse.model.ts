import { BallModel } from "./ball.model";

export class CheckBordersResponseModel {
    ball: BallModel;
    collisionCounter: number;
}