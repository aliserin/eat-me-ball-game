export class SettingsModel {
    gravity: number;        // acceleration constant for the gravity
    ballSize: number;       // radius of the ball
    ballColor: string;      // ball color
    health: number;         // number of the collision of the wall for eat me game
    isSoundOpen: boolean;   // turn on and off the sound
}