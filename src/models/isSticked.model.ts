// model for checking is ball already collides to walls
export class IsSticked {
    left: boolean;      // left wall
    right: boolean;     // right wall
    top: boolean;       // top wall
    bottom: boolean;    // bottom wall
}