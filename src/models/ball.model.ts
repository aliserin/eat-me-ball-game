import { PointModel } from "./point.model";

export class BallModel {
    point: PointModel;      // position of the ball
    velocity?: PointModel;  // velocity of the ball
    r: number;              // radius of the ball
    color: string;          // color of the ball
}