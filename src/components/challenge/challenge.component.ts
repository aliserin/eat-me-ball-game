import { Component, AfterViewInit, ViewChild, Renderer, OnDestroy } from "@angular/core";
import { DrawingService } from "../../services/drawing.service";
import { DeviceMotion, DeviceMotionAccelerationData } from "@ionic-native/device-motion";
import { Storage } from '@ionic/storage';
import { BallModel } from "../../models/ball.model";
import { SettingsModel } from "../../models/settings.model";
import { Platform } from "ionic-angular";
import { CheckObstacleService } from "../../services/checkObstacle.service";
import { CheckBordersResponseModel } from "../../models/checkBordersResponse.model";
import { NativeAudio } from '@ionic-native/native-audio'
import { PointModel } from "../../models/point.model";

@Component({
    selector: 'challenge',
    templateUrl: 'challenge.component.html'
})
export class ChallengeComponent implements AfterViewInit, OnDestroy {
    @ViewChild('challengeCanvas') canvas;
    canvasElement: any;
    ctx: any;
    settings: SettingsModel = new SettingsModel();
    startPoint: PointModel = new PointModel();
    startVelocity: PointModel = new PointModel();
    intervalTime: number = 20;
    interval: any;
    collisionCounter: number = 0;

    constructor(public renderer: Renderer,
        private drawingService: DrawingService,
        private checkObstacleService: CheckObstacleService,
        private deviceMotion: DeviceMotion,
        private storage: Storage,
        private platform: Platform,
        private nativeAudio: NativeAudio) {
    }

    ngOnDestroy(): void {
        clearInterval(this.interval);
    }

    ngAfterViewInit(): void {
        this.loadSounds();
        this.loadPage();
    }

    loadPage(): void {
        this.canvasElement = this.canvas.nativeElement;

        this.renderer.setElementAttribute(this.canvasElement, 'width', this.platform.width() * 0.8 + '');
        this.renderer.setElementAttribute(this.canvasElement, 'height', '300');

        this.startPoint = {
            x: this.canvasElement.width / 2,
            y: this.canvasElement.height / 2
        }

        this.startVelocity = { x: 0, y: 0 };

        this.ctx = this.canvasElement.getContext('2d');

        // get settings
        this.storage.get('settings')
            .then(result => {
                this.settings = result;
                this.startMotion();
            });
    }

    // clear canvas
    clearCtx(): void {
        this.ctx.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
    }

    startMotion(): void {
        let ball: BallModel = {
            point: this.startPoint,
            color: this.settings.ballColor,
            r: this.settings.ballSize,
            velocity: this.startVelocity
        }

        this.interval = setInterval(() => {
            // get motion from phone
            let subscription = this.deviceMotion.watchAcceleration().subscribe((acceleration: DeviceMotionAccelerationData) => {
                // add acceleration to velocity of the ball
                ball.velocity.x = ball.velocity.x - acceleration.x * this.settings.gravity;
                ball.velocity.y = ball.velocity.y + acceleration.y * this.settings.gravity;

                this.clearCtx();

                this.drawingService.drawBall(this.ctx, ball)

                // add velocity to position of the ball
                ball.point.x += ball.velocity.x;
                ball.point.y += ball.velocity.y;

                // get new conditions of the ball
                let checkBordersResponse: CheckBordersResponseModel =
                    this.checkObstacleService.checkBorders(ball, this.canvasElement.width, this.canvasElement.height);
                
                ball = checkBordersResponse.ball;
                this.collisionCounter += checkBordersResponse.collisionCounter;

                for (let i = 0; i < checkBordersResponse.collisionCounter; i++) {
                    this.playSound('bounce');
                }
            });
            subscription.unsubscribe();
        }, this.intervalTime)
    }

    reload(): void {
        this.clearCtx();
        clearInterval(this.interval);
        this.collisionCounter = 0;
        this.loadPage();
    }

    loadSounds(): void {
        this.nativeAudio.preloadSimple('bounce', 'assets/sounds/bounce.mp3');
    }

    playSound(soundString: string): void {
        if (this.settings.isSoundOpen) this.nativeAudio.play(soundString);
    }
}