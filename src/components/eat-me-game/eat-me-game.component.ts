import { Component, AfterViewInit, ViewChild, Renderer, OnDestroy } from "@angular/core";
import { DrawingService } from "../../services/drawing.service";
import { DeviceMotion, DeviceMotionAccelerationData } from "@ionic-native/device-motion";
import { Storage } from '@ionic/storage';
import { BallModel } from "../../models/ball.model";
import { SettingsModel } from "../../models/settings.model";
import { Platform, AlertController } from "ionic-angular";
import { CheckObstacleService } from "../../services/checkObstacle.service";
import { CheckBordersResponseModel } from "../../models/checkBordersResponse.model";
import { NativeAudio } from '@ionic-native/native-audio'
import { PointModel } from "../../models/point.model";

@Component({
    selector: 'eat-me-game',
    templateUrl: 'eat-me-game.component.html'
})
export class EatMeGameComponent implements AfterViewInit, OnDestroy {
    @ViewChild('eatMeCanvas') canvas;
    canvasElement: any;
    ctx: any;
    settings: SettingsModel = new SettingsModel();
    startPoint: PointModel = new PointModel();
    startVelocity: PointModel = new PointModel();
    intervalTime: number = 20;
    interval: any;
    isEated: boolean = false;
    score: number = 0;
    scorePlus: number = 10;
    eatingPlus: number = 3;
    collisionCounter: number = 0;
    health: number;
    highScore: number = 0;

    constructor(public renderer: Renderer,
        private drawingService: DrawingService,
        private checkObstacleService: CheckObstacleService,
        private deviceMotion: DeviceMotion,
        private storage: Storage,
        private platform: Platform,
        private alertCtrl: AlertController,
        private nativeAudio: NativeAudio) {
    }

    ngOnDestroy(): void {
        clearInterval(this.interval);
    }

    ngAfterViewInit(): void {
        this.loadSounds();
        this.loadPage()
    }

    loadPage() {
        this.canvasElement = this.canvas.nativeElement;

        this.renderer.setElementAttribute(this.canvasElement, 'width', this.platform.width() * 0.8 + '');
        this.renderer.setElementAttribute(this.canvasElement, 'height', '300');

        this.startPoint = {
            x: this.canvasElement.width / 2,
            y: this.canvasElement.height / 2
        }

        this.startVelocity = { x: 0, y: 0 };

        this.ctx = this.canvasElement.getContext('2d');

        // get settings
        this.storage.get('settings')
            .then(result => {
                this.settings = result;
                this.health = this.settings.health;

                // get high score
                this.storage.get('highScore')
                    .then(highScoreResult => {
                        if (highScoreResult != null) {
                            this.highScore = highScoreResult;
                        }
                    })
                this.startMotion();
            });
    }

    // clear canvas
    clearCtx() {
        this.ctx.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
    }

    startMotion() {
        let ball: BallModel = {
            point: this.startPoint,
            color: this.settings.ballColor,
            r: this.settings.ballSize,
            velocity: this.startVelocity
        }

        // get random ball for eating
        let foodBall = this.drawingService.createRandomBall(ball, this.canvasElement.width, this.canvasElement.height);

        this.interval = setInterval(() => {
            // get motion from phone
            let subscription = this.deviceMotion.watchAcceleration().subscribe((acceleration: DeviceMotionAccelerationData) => {
                // add acceleration to velocity of the ball
                ball.velocity.x = ball.velocity.x - acceleration.x * this.settings.gravity;
                ball.velocity.y = ball.velocity.y + acceleration.y * this.settings.gravity;

                this.clearCtx();

                this.drawingService.drawBall(this.ctx, foodBall);
                this.drawingService.drawBall(this.ctx, ball);

                // add velocity to position of the ball
                ball.point.x += ball.velocity.x;
                ball.point.y += ball.velocity.y;

                // get new conditions of the ball
                let checkBordersResponse: CheckBordersResponseModel =
                    this.checkObstacleService.checkBorders(ball, this.canvasElement.width, this.canvasElement.height);

                // decrease health if the ball hits the wall
                this.health -= checkBordersResponse.collisionCounter;
                for (let i = 0; i < checkBordersResponse.collisionCounter; i++) {
                    this.playSound('bounce');
                }
                if (this.health <= 0) {
                    clearInterval(this.interval);

                    // finish the game if there is no health
                    this.gameOver();
                }

                // get eating info
                this.isEated = this.checkObstacleService.isFoodBallEated(ball, foodBall);

                if (this.isEated) {
                    this.playSound('eat');
                    ball.r += this.eatingPlus;
                    this.score += this.scorePlus;

                    // get new random ball
                    foodBall = this.drawingService.createRandomBall(ball, this.canvasElement.width, this.canvasElement.height);
                }
            })
            subscription.unsubscribe();
        }, this.intervalTime)
    }

    reload() {
        this.clearCtx();
        clearInterval(this.interval);
        this.score = 0;
        this.health = 10;
        this.collisionCounter = 0;
        this.loadPage();
    }

    gameOver() {
        if (this.score > this.highScore) this.setHighScore();

        let alert = this.alertCtrl.create({
            title: 'GAME OVER!',
            subTitle: 'Score: ' + this.score + ' pts',
            message: this.score > this.highScore? '--NEW HIGH SCORE--': null,
            buttons: [
                {
                    text: 'Ok',
                    role: 'confirm',
                    handler: () => {
                        this.reload();
                    }
                }
            ]

        });
        alert.present();
    }

    setHighScore() {
        this.storage.set('highScore', this.score)
            .catch(error => {

            })
    }

    loadSounds() {
        this.nativeAudio.preloadSimple('eat', 'assets/sounds/eat.mp3');
        this.nativeAudio.preloadSimple('bounce', 'assets/sounds/bounce.mp3');
    }

    playSound(soundString: string) {
        if (this.settings.isSoundOpen) this.nativeAudio.play(soundString);
    }
}